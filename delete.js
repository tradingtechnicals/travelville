import React, { Component } from 'react';
import logo from './logo.svg';
import M from 'materialize-css';

class App extends Component {
  
  componentDidMount(){
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems, {});
    })
  }

  render() {

    return (
      <div className="App">
          <nav>
            <div class="nav-wrapper">
              <a href="#!" class="brand-logo">Quazzu</a>
              <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
              <ul class="right hide-on-med-and-down">
                  <li><a href="#Home">Home</a></li>
                  <li><a href="#Search.html">Search</a></li>
                  <li><a href="#Popular.html">Popular Places</a></li>
                  <li><a href="#Gallery">Gallery</a></li>
                  <li><a href="#Contact">Contact</a></li>
              </ul>
            </div>
          </nav>
          <ul class="sidenav" id="mobile-nav" ref={Sidenav => this.Sidenav = Sidenav}>
          <h4></h4>
            <li><a href="#Home">Home</a></li>
            <li><a href="#Search.html">Search</a></li>
            <li><a href="#Popular.html">Popular Places</a></li>
            <li><a href="#Gallery">Gallery</a></li>
            <li><a href="#Contact">Contact</a></li>
          </ul>
          <secrtion class="slider" ref={Slider => this.Slider = Slider}>
            <ul className="slides">
              <li>
                <img src="./assets/resort1" alt=""/>
                <div className="caption center-align">
                  <h2>Take Your Dream Vacation</h2>
                  <h5 className="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumque voluptates doloremque!</h5>
                  <a href="#" className="btn btn-large">Learn More</a>
                </div>
              </li>
              <li>
                <img src="./assets/resort2" alt=""/>
                <div className="caption left-align">
                  <h2>We Work With All Budgets</h2>
                  <h5 className="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumque voluptates doloremque!</h5>
                  <a href="#" className="btn btn-large">Learn More</a>
                </div>
              </li>
              <li>
                <img src="./assets/resort3" alt=""/>
                <div className="caption right-align">
                  <h2>Take Your Dream Vacation</h2>
                  <h5 className="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumque voluptates doloremque!</h5>
                  <a href="#" className="btn btn-large">Learn More</a>
                </div>
              </li>
            </ul>
          </secrtion>
      </div>

    );
  }
}

export default App;