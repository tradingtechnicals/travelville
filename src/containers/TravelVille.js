import React, { Component } from 'react';
import M from 'materialize-css';
import '../main.css'
import NavBar from '../components/travelVille/NavBar';
import SideNav from '../components/travelVille/SideNav';
import SliderMain from '../components/travelVille/Slider';
import SearchBar from '../components/travelVille/SearchBar';
import CardsIntro from '../components/travelVille/CardsIntro';
import Popular from '../components/travelVille/Popular';
import SocialMedia from '../components/travelVille/SocialMedia';
import Gallery from '../components/travelVille/Gallery';
import Contact from '../components/travelVille/Contact';
import Footer from '../components/travelVille/Footer';

class TravelVille extends Component {
  
  componentDidMount(){
    M.AutoInit();
    M.Sidenav.init(this.Sidenav, {});
    M.Slider.init(this.Slider,{
      indicators:false,
      height:500, 
      transition:500,
      interval:6000
    });
    M.Autocomplete.init(this.Autocomplete, {
      data:{
        "cancun": null,
      }
    })
   
  }

  render() {

    return (
      <div className="App scrollspy" id="home"  ref={ScrollSpy => this.ScrollSpy = ScrollSpy}>
      <NavBar />
        <SideNav />
          <section className="slider" ref={Slider => this.Slider = Slider}>
            <SliderMain />
          </section>
          <section className="section section-search teal darken-1 white-text center scrollspy" id="search">
            <SearchBar />
          </section>
          <section className="section section-icons grey lighten-4 center">
            <CardsIntro />
          </section>
          <section className="section section-popular scrollspy" id="popular">
            <Popular />
          </section>
          <section className="section section-follow teal darken-2 white-text center">
            <SocialMedia />
          </section>
          <section className="section section-galllery scrollspy" id="gallery">
            <Gallery />
          </section>
          <section className="section section-contact scrollspy" id="contact" ref={ScrollSpy => this.ScrollSpy = ScrollSpy}>
            <Contact />
          </section>
          <Footer />
      </div>

    );
  }
}

export default TravelVille;