import React, { Component } from 'react';
import { SocialIcon } from 'react-social-icons';
import SideNav from '../components/Quazzu/SideNav'
import NavBar from '../components/Quazzu/NavBar'
import M from 'materialize-css';


class Quazzu extends Component {

    componentDidMount(){
        M.AutoInit();
        M.Sidenav.init(this.Sidenav, {});
    }


    render(){
        return(
            <div className="App">
                 <NavBar />
                <SideNav />
                <div className="showcase container center" >
                    <div className="col s12 m10 offset-m1 center" style={{position:'absolute', paddingTop:100, zIndex:100}}>
                        <h5 className="white-text">Welcome To Quazzu</h5>
                        <h1 className="white-text">Build For The Future</h1>
                        <p className="white-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita incidunt illo impedit vel enim? Eveniet!</p>
                        <br/>
                        <a href="solutions.html" className="btn btn-large white purple-text">Learn More</a>
                        <a href="solutions.html" className="btn btn-large purple white-text">Learn More</a>
                    </div>
                </div>
            </div>
        )
    }

}

export default Quazzu;

/* 
                */