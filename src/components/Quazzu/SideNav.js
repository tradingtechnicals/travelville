
import React, { Component } from 'react';

class SideNav extends Component {

    render(){
        return(
            <ul class="sidenav" id="mobile-demo" ref={Sidenav => this.Sidenav = Sidenav}>
                <h4 className='purple-text text-darken-4 center'>Quazzu</h4>
                <li>
                    <div className="divider"></div>
                    <li>
                        <a href="index.html"><i className="material-icons"> face</i>Home</a>
                    </li>
                    <li>
                        <a href="solutions.html"><i className="material-icons">face</i> Solutions</a>
                    </li>
                    <li>
                         <a href="signup.html"><i className="material-icons">face</i> Sign Up</a>
                    </li>
                    <div className="divider"></div>
                    <li>
                        <a href="#" className="btn purple">Login</a>
                    </li>
                </li>
               
            </ul>
        )
    }

}

export default SideNav;
