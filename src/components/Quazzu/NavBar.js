import React, { Component } from 'react';
import { SocialIcon } from 'react-social-icons';
import resort1 from '../../assets/resort1.jpg';
import '../../styling/mainQuazzu.css'


class NavBar extends Component {

render(){
    return(
        <div className="navbar-fixed" style={{zIndex:1}}>
        <nav className="navBarHeader" >
       
        <div className="container">
        <div className="nav-wrapper">
            <a href="#!" className="brand-logo">Quazzu</a>
            <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
            <ul className="right hide-on-med-and-down">
                <li><a href="index.html">Home</a></li>
                <li><a href="solutions.html">Solutions</a></li>
                <li><a href="signup.html">Sign Up</a></li>
                <li>
                    <a href="#" className="btn purple">Login</a>
                </li>
                <li>
                    <a href="https://facebook.com" className="white-text" >
                        <SocialIcon url="http://facebook.com"  bgColor="white" />
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com"  className="white-text">
                    <SocialIcon url="http://twitter.com"  bgColor="white"/>
                    </a>
                </li>
                <li>
                    <a href="https://linkedin.com" className="white-text" >
                    <SocialIcon url="http://linkedin.com"  bgColor="white"/>
                    </a>
                </li>
                <li>
                    <a href="https://googleplus.com" className="white-text" >
                    <SocialIcon url="http://googleplus.com" bgColor="white" />
                    </a>
                </li>
               <li>
                <a href="https://pintrest.com" className="white-text">
                    <SocialIcon url="http://pintrest.com" bgColor="white" />
                    </a>
               </li>
            </ul>
        </div>
        </div>
        </nav>
    </div>
        )
    }
}

export default NavBar;