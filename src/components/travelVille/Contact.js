

import React, { Component } from 'react';

class Contact extends Component {
    render(){
        return(
            <div className="container">
                <div className="row">
                <div className="col s12 m6">
                    <div className="card-panel teal white-text center">
                    <i className="material-icons medium">email</i>
                    <h5>Contact Us for Booking</h5>
                    <p className="">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumqu
                    </p>
                    </div>
                    <ul className="collection with-header">
                    <li className="collection-header">
                    <h4>Location</h4>
                    </li>
                    <li className="collection-item">Travelville Agency</li>
                    <li className="collection-item">555 Beach rd, Suit 33</li>
                    <li className="collection-item">Miami FL, 55555</li>
                </ul>
                </div>
                
                <div className="col s12 m6">
                    <div className="card-panel grey lighten-3">
                    <h5>Please fill out this form</h5>
                    <div className="input-field">
                        <input type="text" placeholder="Name" id="name"/>
                        <label htmlFor="name "></label>
                    </div>
                    <div className="input-field">
                        <input type="email" placeholder="Email" id="Email"/>
                        <label htmlFor="Email "></label>
                    </div>
                    <div className="input-field">
                        <input type="text" placeholder="Phone" id="Phone"/>
                        <label htmlFor="phone "></label>
                    </div>
                    <div className="input-field">
                        <textarea className="materialize-textarea" placeholder="Enter Message" id="message"></textarea>
                        <label htmlFor="message"></label>
                    </div>
                        <input type="submit" value="Submit" className="btn"/>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

export default Contact;

