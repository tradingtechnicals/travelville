
import React, { Component } from 'react';

class SideNav extends Component {

    render(){
        return(
            <ul class="sidenav" id="mobile-demo" ref={Sidenav => this.Sidenav = Sidenav}>
                <li><a href="#home">Home</a></li>
                <li><a href="#search">Search</a></li>
                <li><a href="#popular">Popular Places</a></li>
                <li><a href="#gallery">Gallery</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        )
    }

}

export default SideNav;
