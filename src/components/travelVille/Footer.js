
import React, { Component } from 'react';

class Footer extends Component {
    render(){
        return(
            <footer className="section teal darken-2 white-text center">
                <p className="flow-text">Travelville &copy; 2018</p>
            </footer>
        )
    }
}

export default Footer;



