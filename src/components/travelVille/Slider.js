


import React, { Component } from 'react';

import resort2 from '../../assets/resort2.jpg';
import resort3 from '../../assets/resort3.jpg';
import resort1 from '../../assets/resort1.jpg';


class SliderMain extends Component {
    render(){
        return(
            <ul className="slides">
                <li>
                <img src={resort1}  alt=""/>
                <div className="caption center-align">
                    <h2>Take Your Dream Vacation</h2>
                    <h5 className="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumque voluptates doloremque!</h5>
                    <a href="#"  style={{marginTop:10}} className="btn btn-large">Learn More</a>
                </div>
                </li>
                <li>
                <img src={resort2} alt=""/>
                <div className="caption left-align">
                    <h2>We Work With All Budgets</h2>
                    <h5 className="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumque voluptates doloremque!</h5>
                    <a href="#" style={{marginTop:10}} className="btn btn-large">Learn More</a>
                </div>
                </li>
                <li>
                <img src={resort3} alt=""/>
                <div className="caption right-align">
                    <h2>Take Your Dream Vacation</h2>
                    <h5 className="light grey-text text-lighten-3 hide-on-small-only">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumque voluptates doloremque!</h5>
                    <a href="#" style={{marginTop:10}} className="btn btn-large">Learn More</a>
                </div>
                </li>
                </ul>
        )
    }
}

export default SliderMain;

