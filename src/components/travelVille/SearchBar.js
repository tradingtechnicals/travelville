
import React, { Component } from 'react';

class SearchBar extends Component {

  render(){
    return(
      <div className="container">
        <div className="row">
          <div className="col s12">
              <h3>Search Destinations</h3>
            <div className="input-field">
              <input type="text" ref={Autocomplete => this.Autocomplete = Autocomplete} className="white grey-text autocomplete" placeholder="Aruba, Cancun, etc..." id="autocomplete-input"/>
            </div>
          </div>
        </div>
        </div>
    )
  }

}

export default SearchBar;




