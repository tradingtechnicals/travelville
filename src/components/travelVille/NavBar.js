import React, { Component } from 'react';

class NavBar extends Component {

render(){
    return(
        <div className="navbar-fixed">
        <nav className="teal">
        <div className="container">
          <div class="nav-wrapper">
            <a href="#!" class="brand-logo">Quazzu</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="#home">Home</a></li>
                <li><a href="#search">Search</a></li>
                <li><a href="#popular">Popular Places</a></li>
                <li><a href="#gallery">Gallery</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
          </div>
          </div>
        </nav>
        </div>
        )
    }
}

export default NavBar;