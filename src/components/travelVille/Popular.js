
import React, { Component } from 'react';

import resort2 from '../../assets/resort2.jpg';
import resort3 from '../../assets/resort3.jpg';
import resort1 from '../../assets/resort1.jpg';


class Popular extends Component {
    render(){
        return(
            <div className="container">
            <div className="row">
            <h4 className="center"><span className='teal-text'>Popular</span> Places</h4>
            <div className="col s12 m4">
                <div className="card">
                <div className="card-image">
                    <img src={resort1} alt=""/>
                    <span className="card-title">Cancun, Mexico</span>
                </div>
                <div className="card-content">
                    <p className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumqu
                    </p>
                </div>
                </div>
            </div>
            <div className="col s12 m4">
                <div className="card">
                <div className="card-image">
                    <img src={resort2} alt=""/>
                    <span className="card-title">The Bahamas</span>
                </div>
                <div className="card-content">
                    <p className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumqu
                    </p>
                </div>
                </div>
            </div>
            <div className="col s12 m4">
                <div className="card">
                <div className="card-image">
                    <img src={resort1} alt=""/>
                    <span className="card-title">Nova Scotia</span>
                </div>
                <div className="card-content">
                    <p className="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut rem architecto mollitia cumqu
                    </p>
                </div>
                </div>
            </div>
            </div>
            <div className="row">
            <div className="col s12 center">
                <a href="#contact" className="btn btn-large grey darken-3">
                <i className="material-icons left">send</i>
                Contact for booking
                </a>
            </div>
            </div>
            </div>
        )
    }
}

export default Popular;




