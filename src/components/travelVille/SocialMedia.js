
import React, { Component } from 'react';
import { SocialIcon } from 'react-social-icons';



class SocialMedia extends Component {
    render(){
        return(
        <div className="container">
            <div className="row">
            <div className="col s12">
                <h4>Follow Travelville</h4>
                <p>Follow us on social media for special offers</p>
                <a href="//https://facebook.com" className="white-text" style={{marginTop:5,marginBottom:5, marginLeft:10, marginRight:10}}>
                <SocialIcon url="http://facebook.com"  bgColor="white"/>
                </a>
                <a href="//https://twitter.com"  className="white-text" style={{marginTop:5,marginBottom:5, marginLeft:10, marginRight:10}}>
                <SocialIcon url="http://twitter.com"  bgColor="white"/>
                </a>
                <a href="//https://linkedin.com" className="white-text" style={{marginTop:5,marginBottom:5, marginLeft:10, marginRight:10}}>
                <SocialIcon url="http://linkedin.com"  bgColor="white"/>
                </a>
                <a href="//https://googleplus.com" className="white-text" style={{marginTop:5,marginBottom:5, marginLeft:10, marginRight:10}}>
                <SocialIcon url="http://googleplus.com"  bgColor="white"/>
                </a>
                <a href="//https://pintrest.com" className="white-text" style={{marginTop:5,marginBottom:5, marginLeft:10, marginRight:10}}>
                <SocialIcon url="http://pintrest.com" bgColor="white" />
                </a>
            </div>
            </div>
        </div>
        )
    }
}

export default SocialMedia;


